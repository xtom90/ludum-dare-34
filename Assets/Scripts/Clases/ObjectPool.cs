﻿/* Realizado por Tomas Bruckner Kambo. Caracas, Venezuela 2015 */
/* La clase auxiliar o "helper" Object Pool tiene la finalidad 
 * de brindad de manera comoda un manejo de pools de objetos.*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool {
	
	/* Clase auxiliar que permite crear objetos de manera eficiente */
	public Queue<GameObject> cola; //cola con los objetos ya creados
	public string id;
	
	/* Constructor */
	public ObjectPool(int tamano, GameObject prefab) {
		cola = new Queue<GameObject>(); //inicializamos la cola
		for(int i = 0; i < tamano ; i++){ //añadimos a la cola la cantidad de objetos necesarios
			GameObject objeto = MonoBehaviour.Instantiate(prefab) as GameObject; //Creamos el objeto como tal
			objeto.SetActive(false); //inicialmente el objeto esta inactivo
			cola.Enqueue(objeto); //encolamos el objeto
		}
	}
	
	/* Destruir Objeto*/
	public void destruirObjeto(GameObject objeto){
		objeto.SetActive(false);
		cola.Enqueue(objeto);
	}
	
	/* Crear Objeto */
	public GameObject crearObjeto(){
		GameObject objeto = cola.Dequeue(); //sacamos de la cola
		objeto.SetActive(true); //activamos el objeto
		return objeto; 
	}
	
	/* Limpiar Pool */
	public void limpiarCola() {
		int tamano = cola.Count; //obtenemos la cantidad de elementos
		for (int i = 0; i < tamano ; i++) { 
			GameObject.Destroy(cola.Dequeue());
		}
		cola = null;
	}
	
	/* Obtener cantidad de hijos */
	public int nHijos () {
		return cola.Count;
	}
}

