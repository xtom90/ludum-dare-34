﻿using UnityEngine;
using System.Collections;

public class Resetear : MonoBehaviour {

	private Vector3 posI, tamI;
	private Quaternion rotI;
	private SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
		sprite = transform.GetChild(0).GetComponent<SpriteRenderer> ();
		posI = transform.localPosition;
		rotI = transform.localRotation;
		tamI = transform.localScale;
	}
	
	public void resetear(){
		transform.localPosition = posI;
		transform.localRotation = rotI;
		transform.localScale = tamI;
		sprite.color = Color.white;
	}
}
