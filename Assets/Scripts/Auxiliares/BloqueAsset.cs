﻿using UnityEngine;
using System.Collections;

public class BloqueAsset : MonoBehaviour {

	private Administracion admin;
	private Generar generar;

	void Awake () {
		admin = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<Administracion> ();
		StartCoroutine (verificacion ());
	}
	
	// Update is called once per frame
	void Update () {
		movimiento ();
	}

	void movimiento(){
		transform.position += Vector3.left * Time.deltaTime * admin.velocidadBloques;
	}

	/* CORUTINAS */
	IEnumerator verificacion(){
		while(transform.position.x > -admin.limiteX / 2f - Tags.anchoBloque){
			yield return null;
		}
		Destroy (gameObject, 0f);
	}
}
