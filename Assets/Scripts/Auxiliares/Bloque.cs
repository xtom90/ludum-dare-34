﻿using UnityEngine;
using System.Collections;

public class Bloque : MonoBehaviour {

	public bool tieneExtra = false;
	public bool habilitado = true;

	private ObjectPool objPool;
	private Administracion admin;
	private Generar generar;
	private Resetear[] res;

	// Use this for initialization
	void Awake () {
		admin = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<Administracion> ();
		generar = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<Generar> ();
		if (tieneExtra) {
			res = GetComponentsInChildren<Resetear>();
		} else {
			res = null;
		}
	}
	// Update is called once per frame
	void Update () {
		movimiento ();
	}


	void movimiento(){
		if (habilitado) {
			transform.position += Vector3.left * Time.deltaTime * admin.velocidadBloques;
		}
	}

	void resetear(){
		if(tieneExtra){
			foreach(Resetear r in res){
				r.resetear();
			}
		}
	}

	public void crearBloque(Vector3 pos, ObjectPool oPool){
		transform.position = pos;
		objPool = oPool;
		StartCoroutine (verificacion());
	}

	public void destruirBloque(){
		transform.position = new Vector3(100f, 100f, 100f);
		resetear ();
		objPool.destruirObjeto(gameObject);
	}

	/* CORUTINAS */
	IEnumerator verificacion(){
		while(transform.position.x > 0f){
			yield return null;
		}
		generar.crearBloque ();
		while(transform.position.x > -admin.limiteX / 2f - Tags.anchoBloque){
			yield return null;
		}
		admin.aumentarRecorrido ();
		destruirBloque ();
	}
}
