﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {

	/* TAGS */
	public const string camaraPpal = "MainCamera";
	public const string jugador = "Player";
	public const string gameController = "GameController";
	public const string limites = "Limites";
	public const string limiteIzq = "LimiteIzq";
	public const string limiteDer = "LimiteDer";
	public const string limiteInf = "LimiteInf";
	public const string powerUp = "PowerUp";
	public const string enemigos = "Enemigo";
	public const string interfaz = "Interfaz";
	public const string bloque = "Bloque";

	/* LAYERS */

	/* CONSTANTES */
	public const float anchoBloque = 10f;

	/* PARAMETROS */
}
