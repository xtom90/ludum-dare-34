﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Splash : MonoBehaviour {

	private Image logo;

	// Use this for initialization
	void Start () {
		logo = GetComponent<Image> ();
		logo.color = Color.clear;
		StartCoroutine (fadeInFadeOut ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator fadeInFadeOut(){
		yield return new WaitForSeconds(1f);
		while (logo.color.a < 0.9f) {
			logo.color = Color.Lerp(logo.color, Color.white, 0.05f);
			yield return null;
		}
		logo.color = Color.white;
		yield return new WaitForSeconds(6f);
		while (logo.color.a > 0.1f) {
			logo.color = Color.Lerp(logo.color, Color.clear, 0.05f);
			yield return null;
		}
		logo.color = Color.clear;
		yield return new WaitForSeconds(1f);
		Application.LoadLevel ("Menu");
	}
}
