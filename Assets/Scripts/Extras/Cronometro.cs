﻿using UnityEngine;
using System.Collections;

public class Cronometro : MonoBehaviour {

	public GameObject fondo;
	public float duracion = 87f;
	public string sigNivel = "";

	// Use this for initialization
	void Start () {
		StartCoroutine (cronom ());
	}

	void Update(){
		if(Input.GetKey(KeyCode.Escape)){
			Application.LoadLevel (sigNivel);
		}
	}

	
	IEnumerator cronom(){
		yield return new WaitForSeconds (duracion);
		Invoke ("oscurecer", 0f);
		yield return new WaitForSeconds (1f);
		Application.LoadLevel (sigNivel);
	}

	void oscurecer(){
		fondo.GetComponent<OscurecerAclarar> ().oscurecer ();
	}
}
