﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;


public class BotonControles : MonoBehaviour, IPointerClickHandler {

	public GameObject modal;

	void Start(){
		modal.SetActive (false);
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		modal.SetActive (true);
	}

	#endregion
}
