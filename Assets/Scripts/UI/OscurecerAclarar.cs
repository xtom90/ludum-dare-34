﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class OscurecerAclarar : MonoBehaviour {

	private Image img;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image> ();
		StartCoroutine (aclararCorutina ());
	}

	public void oscurecer(){
		StartCoroutine (oscurecerCorutina ());
	}

	public void aclarar(){
		StartCoroutine (aclararCorutina ());
	}

	public void completo(){
		StartCoroutine (aclararCorutina ());
	}

	IEnumerator completoCorutina(){
		while(img.color.a < 0.9f){
			img.color = Color.Lerp(img.color, Color.white, 0.1f);
			yield return null;
		}
		img.color = Color.white;
		while(img.color.a > 0.1f){
			img.color = Color.Lerp(img.color, Color.clear, 0.1f);
			yield return null;
		}
		img.color = Color.clear;
	}
	
	IEnumerator aclararCorutina(){
		while(img.color.a > 0.1f){
			img.color = Color.Lerp(img.color, Color.clear, 0.1f);
			yield return null;
		}
		img.color = Color.clear;
	}

	IEnumerator oscurecerCorutina(){
		while(img.color.a < 0.9f){
			img.color = Color.Lerp(img.color, Color.white, 0.1f);
			yield return null;
		}
		img.color = Color.white;
	}
}
