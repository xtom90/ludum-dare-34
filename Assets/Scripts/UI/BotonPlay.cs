﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;


public class BotonPlay : MonoBehaviour, IPointerClickHandler {

	public GameObject seleccion, inicio;

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		seleccion.SetActive (true);
		inicio.SetActive (false);
	}
	#endregion
}
