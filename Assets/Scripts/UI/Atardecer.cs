﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Atardecer : MonoBehaviour {

	private RawImage[] fondos;
	public Color color;

	// Use this for initialization
	void Start () {
		fondos = transform.GetComponentsInChildren<RawImage>();
		//color = new Color (126f, 64f, 255f);
	}

	public void iniciar(){
		StartCoroutine (atardecerCorutina ());
	}

	IEnumerator atardecerCorutina(){
		while(fondos[0].color != color){
			Debug.Log ("HOLI");
			foreach(RawImage fondo in fondos){
				fondo.color += Color.Lerp(fondo.color, color, 0.5f);
			}
			yield return null;
		}
	}
}
