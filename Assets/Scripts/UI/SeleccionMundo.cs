﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;


public class SeleccionMundo: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

	public string nivel = "";
	public GameObject fondo;
	private Parallax par;

	// Use this for initialization
	void Start () {
		par = transform.GetChild (0).GetComponent<Parallax> ();
		par.enabled = false;
	}

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		par.enabled = true;
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		par.enabled = false;
	}

	#endregion

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		par.enabled = false;
		fondo.GetComponent<OscurecerAclarar> ().oscurecer ();
		Invoke ("irNivel", 2f);
	}

	#endregion

	void irNivel(){
		Application.LoadLevel (nivel);
	}
}
