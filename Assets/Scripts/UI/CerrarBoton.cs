﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CerrarBoton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

	public GameObject modal;

	private Text cerrar;

	// Use this for initialization
	void Start () {
		cerrar = GetComponent<Text> ();
		cerrar.color = Color.white;
	}

	#region IPointerEnterHandler implementation
	public void OnPointerEnter (PointerEventData eventData)
	{
		cerrar.color = Color.red;
	}
	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		cerrar.color = Color.white;
	}

	#endregion

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		cerrar.color = Color.white;
		modal.SetActive (false);
	}

	#endregion
}
