﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class AnimOutline : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	private Outline outline;

	// Use this for initialization
	void Start () {
		outline = GetComponent<Outline> ();
		outline.effectColor = Color.clear;
	}

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		//outline.effectColor = Color.Lerp (outline.effectColor, Color.white, 0.3f);
		outline.effectColor = Color.white;
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		outline.effectColor = Color.clear;
	}

	#endregion
}
