﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Parallax : MonoBehaviour {

	public float velocidad = 1f;

	private RawImage img;

	// Use this for initialization
	void Start () {
		img = GetComponent<RawImage> ();
	}
	
	// Update is called once per frame
	void Update () {
		float x = img.uvRect.x + Time.deltaTime * velocidad;
		img.uvRect = new Rect (x, 0, 1, 1);
	}
}
