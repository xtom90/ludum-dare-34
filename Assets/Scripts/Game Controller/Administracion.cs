﻿using UnityEngine;
using System.Collections;

public class Administracion : MonoBehaviour {

	public float velocidadBloques = 1f;
	public float limiteX, limiteY;
	public string sigNivel = "";

	private Camera camara;
	private Transform limites;
	private int recorrido, recorridoRealizado;
	private Generar generar;
	private Jugador jugador;
	private Atardecer atardecer;
	private Bloque[] bloquesScripts;

	// Use this for initialization
	void Start () {
		generar = GetComponent<Generar> ();
		atardecer = GameObject.FindGameObjectWithTag (Tags.interfaz).GetComponent<Atardecer>();
		camara = GameObject.FindGameObjectWithTag (Tags.camaraPpal).GetComponent<Camera>();
		jugador = GameObject.FindGameObjectWithTag (Tags.jugador).GetComponent<Jugador> ();
		limiteY = 2.0f * Mathf.Abs(camara.transform.position.z) * Mathf.Tan(camara.fieldOfView * 0.5f * Mathf.Deg2Rad); //Obtenemos el tamaño horizontal
		limiteX = limiteY * camara.aspect;  //Obtenemos el tamaño vertical
		limites = GameObject.FindGameObjectWithTag (Tags.limites).GetComponent<Transform> ();
		limites.GetChild (0).transform.position = new Vector3 (-limiteX/2f, 0f, 0f);
		limites.GetChild (1).transform.position = new Vector3 (limiteX/2f, 0f, 0f);
		recorrido = generar.obtenerLongitudMundo ();
		recorridoRealizado = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void gameOver(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void victoria(){
		Debug.Log ("GANASTE");
		generar.secuenciaFinal ();
		jugador.animacionFinal ();
		//atardecer.iniciar ();
	}

	public void recalcularLimites(){
		limiteY = 2.0f * Mathf.Abs(camara.transform.position.z) * Mathf.Tan(camara.fieldOfView * 0.5f * Mathf.Deg2Rad); //Obtenemos el tamaño horizontal
		limiteX = limiteY * camara.aspect;
	}

	public void aumentarRecorrido(){
		recorridoRealizado++;
		if(recorridoRealizado == recorrido - 2){
			victoria();
		}
	}

	public void detenerGeneracion(){
		GameObject[] bloques = GameObject.FindGameObjectsWithTag (Tags.bloque);
		bloquesScripts = new Bloque[bloques.Length];
		for(int i = 0; i < bloques.Length; i++){
			bloquesScripts[i] = bloques[i].GetComponent<Bloque>();
			bloquesScripts[i].habilitado = false;
		}
	}

	public void reiniciarGeneracion(){

		GameObject[] bloques = GameObject.FindGameObjectsWithTag (Tags.bloque);
		bloquesScripts = new Bloque[bloques.Length];
		foreach(GameObject bloque in bloques){
			bloque.GetComponent<Bloque>().habilitado = true;
		}
	}
}
