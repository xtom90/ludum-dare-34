﻿using UnityEngine;
using System.Collections;

public class Status : MonoBehaviour {

	private int score;
	private Interfaz gui;

	// Use this for initialization
	void Start () {
		gui = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<Interfaz> ();
		score = 0;
	}
	
	public void aumentarScore(){
		score++;
		gui.aumentarScore ();
	}
}
