﻿using UnityEngine;
using System.Collections;

public class Generar : MonoBehaviour {

	public GameObject[] bloques;
	public int[] secuencia;
	public GameObject[] assetsFondo;

	private Transform limiteIzq, limiteDer;
	private ObjectPool[] pools;
	private int contadorSeq, contadorAssets;
	private bool[] metaObjetos;

	// Use this for initialization
	void Awake () {
		limiteIzq = GameObject.FindGameObjectWithTag(Tags.limiteIzq).transform;
		limiteDer = GameObject.FindGameObjectWithTag(Tags.limiteDer).transform;
		pools = new ObjectPool[bloques.Length];
		metaObjetos = new bool[bloques.Length];
		verificacionMeta ();
		for(int i = 0; i < pools.Length; i++){
			if(metaObjetos[i]){
				pools[i] = new ObjectPool(3, bloques[i]);
			} else {
				pools[i] = null;
			}
		}
		contadorSeq = 0;
		contadorAssets = 0;
	}

	void Start(){
		primerBloque ();
	}

	void primerBloque(){
		int indice = secuencia [contadorSeq];
		GameObject bloq = pools [indice].crearObjeto ();
		bloq.GetComponent<Bloque>().crearBloque(Vector3.zero + Vector3.right * 0.1f, pools [indice]);
		contadorSeq++;
	}

	void generarAsset(){
		Vector3 pos = limiteDer.position + Tags.anchoBloque * 0.5f * Vector3.right + Vector3.forward;
		GameObject asset = Instantiate (assetsFondo [contadorAssets], pos, transform.rotation) as GameObject;
		contadorAssets++;
		if (contadorAssets == assetsFondo.Length) {
			contadorAssets = 0;
		}
	}

	/* Verifico que bloques se usan y cuales no */
	void verificacionMeta(){
		for(int i = 0; i < metaObjetos.Length; i++){
			for(int j = 0; j < secuencia.Length; j++){
				metaObjetos[i] = false;
				if(secuencia[j] == i){
					metaObjetos[i] = true;
					break;
				} 
			}
		}
	}

	public void crearBloque(){
		int indice = secuencia [contadorSeq];
		GameObject bloq = pools [indice].crearObjeto ();
		bloq.GetComponent<Bloque>().crearBloque(limiteDer.position + Tags.anchoBloque * 0.5f * Vector3.right, pools [indice]);
		contadorSeq++;
		if(contadorSeq == secuencia.Length){
			contadorSeq = 0;
		}
		if (contadorSeq % 4 == 0f) {

			generarAsset();
		}
	}

	public int obtenerLongitudMundo(){
		return secuencia.Length;
	}

	public void secuenciaFinal(){
		contadorSeq = 0;
		secuencia = new int[]{0, 0, 0};
	}
}
