﻿using UnityEngine;
using System.Collections;

public class Seguir : MonoBehaviour {

	public float ajuste = 1f;
	public float velocidad = 0.5f;
	private Transform jugador;

	// Use this for initialization
	void Start () {
		jugador = GameObject.FindGameObjectWithTag (Tags.jugador).transform;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 nPos = new Vector3 (transform.position.x, jugador.position.y + 0.5f, transform.position.z);
		transform.position = Vector3.Lerp (transform.position, nPos, velocidad);
	}
}
