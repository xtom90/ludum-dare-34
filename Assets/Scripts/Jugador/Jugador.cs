﻿using UnityEngine;
using System.Collections;

public class Jugador: MonoBehaviour {

	public float velocidad = 10f;
	public float tiempoBerserker = 16f;
	public AudioClip salto, berserkerSFX, berserkerMusica;


	private Rigidbody rb;
	private bool habilitado, grounded, roll;
	private Administracion admin;
	private BoxCollider col;
	private SphereCollider colCircular;
	private Animator anim;
	private SpriteRenderer spriteJugador;
	private bool berserk, berserkSoundFlag;
	private AudioSource musica;
	private AudioClip musicaOriginal;

	// Use this for initialization
	void Start () {
		admin = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<Administracion> ();
		musica = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<AudioSource> ();
		musicaOriginal = musica.clip;
		rb = GetComponent<Rigidbody> ();
		col = GetComponent<BoxCollider> ();
		colCircular = GetComponent<SphereCollider> ();
		spriteJugador = GetComponent<SpriteRenderer> ();
		colCircular.enabled = false;
		col.enabled = true;
		anim = GetComponent<Animator> ();
		habilitado = true;
		grounded = false;
		roll = false;
		berserk = false;
		berserkSoundFlag = false;
	}
	
	// Update is called once per frame
	void Update () {
		enElPiso ();
		if(habilitado && grounded){
			if(Input.GetKey(KeyCode.Space) && !roll){
				AudioSource.PlayClipAtPoint(salto, transform.position);
				rb.velocity = Vector3.up * velocidad;
				grounded = false;
			} 

			if(Input.GetKey(KeyCode.DownArrow)){
				col.enabled = false;
				colCircular.enabled = true;
				anim.SetBool("Roll", true);
				roll = true;
			} else {
				col.enabled = true;
				colCircular.enabled = false;
				anim.SetBool("Roll", false);
				roll = false;
			}
		}
		if(Input.GetKey(KeyCode.Escape)){
			Application.LoadLevel ("Menu");
		}
		verificacionBerserk ();
	}

	void OnTriggerEnter(Collider otro){
		if(otro.tag == Tags.limiteIzq || otro.tag == Tags.limiteInf){
			admin.gameOver();
		} else if(otro.tag == Tags.enemigos){
			if(berserk){
				otro.GetComponent<Enemigo>().morir();
			} else {
				admin.gameOver();
			}
		} else if(otro.tag == Tags.powerUp){
			modoBerserker(otro.transform, otro.transform.GetChild(0).GetComponent<SpriteRenderer>());
		}
	}

	void OnCollisionEnter(Collision collision) {
		grounded = true;      
	}

	void verificacionBerserk(){
		if (berserk) {
			admin.reiniciarGeneracion ();
		}
	}

	void enElPiso(){
		anim.SetBool ("Grounded", grounded);
	}

	void modoBerserker(Transform gota, SpriteRenderer sprite){
		if(!berserkSoundFlag){
			berserkSoundFlag = true;
			AudioSource.PlayClipAtPoint(berserkerSFX, transform.position);
		}
		admin.detenerGeneracion ();
		StartCoroutine (berserkerCorutina1(gota, sprite));
	}

	public void habilitarMov(bool b){
		habilitado = b;
	}

	public void animacionFinal(){
		habilitarMov (false);
		anim.SetTrigger ("Final");
		StartCoroutine (finalCorutina ());
	}

	/* CO-RUTINAS */

	IEnumerator finalCorutina(){
		while(transform.position.x < admin.limiteX / 2f + 1f){
			transform.position += Vector3.right * Time.deltaTime;
			yield return null;
		}
		rb.useGravity = false;
		Application.LoadLevel (admin.sigNivel);
	}

	IEnumerator berserkerCorutina1(Transform gota, SpriteRenderer sprite){
		while(Vector3.Distance(transform.position, gota.position) > 0.1f){
			float transicion = 0.1f;
			transform.position = Vector3.Lerp(transform.position, gota.position, transicion);
			gota.localScale = Vector3.Lerp(gota.localScale, new Vector3(2f, 2f, 2f), transicion);
			yield return null;
		}
		StartCoroutine (berserkerCorutina2(gota, sprite));
	}

	IEnumerator berserkerCorutina2(Transform gota, SpriteRenderer sprite){
		Vector3 posDeseada = new Vector3 (0f, gota.position.y, gota.position.z);
		while(Vector3.Distance(transform.position, posDeseada) > 0.1f){
			float transicion = 0.1f;
			transform.position = Vector3.Lerp(transform.position, posDeseada, transicion);
			gota.position = Vector3.Lerp(gota.position, posDeseada, transicion);
			yield return null;
		}
		yield return new WaitForSeconds(1.5f);
		StartCoroutine (berserkerCorutina3(gota, sprite));
	}

	IEnumerator berserkerCorutina3(Transform gota, SpriteRenderer sprite){
		Vector3 tamDeseado = new Vector3 (2.3f, 2.3f, 2.3f);
		while(Vector3.Distance(gota.localScale, tamDeseado) > 0.1f){
			float transicion = 0.1f;
			gota.localScale = Vector3.Lerp(gota.localScale, tamDeseado, transicion);
			yield return null;
		}
		StartCoroutine (berserkerCorutina4(gota, sprite));
	}

	IEnumerator berserkerCorutina4(Transform gota, SpriteRenderer sprite){
		while(gota.localScale.x > 0.3f){
			float transicion = 0.1f;
			sprite.color = Color.Lerp(sprite.color, Color.clear, transicion);
			gota.localScale = Vector3.Lerp(gota.localScale, Vector3.zero, transicion);
			yield return null;
		}
		sprite.color = Color.clear;
		gota.localScale = Vector3.one;
		berserk = true;
		spriteJugador.color = Color.red;
		berserkSoundFlag = false;
		StartCoroutine (timeoutBerserker());
	}

	IEnumerator cambioColor(Transform gota, SpriteRenderer sprite){
		if (spriteJugador.color == Color.yellow) {
			spriteJugador.color = Color.red;
		} else {
			spriteJugador.color = Color.yellow;
		}
		yield return new WaitForSeconds(0.5f);
		yield return StartCoroutine (cambioColor(gota, sprite));
	}

	IEnumerator timeoutBerserker(){
		musica.clip = berserkerMusica;
		musica.Play ();
		yield return new WaitForSeconds (tiempoBerserker);
		musica.clip = musicaOriginal;
		musica.Play ();
		berserk = false;
		spriteJugador.color = Color.white;
	}
}
