﻿using UnityEngine;
using System.Collections;

public class Enemigo : MonoBehaviour {

	public float velocidad = 5f;
	private Vector3 posI;

	// Use this for initialization
	void Start () {
		posI = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void morir(){
		StartCoroutine (morirCorutina());
	}

	IEnumerator morirCorutina(){
		while (transform.position.y > -15f){
			transform.position += Vector3.down * Time.deltaTime * velocidad;
			transform.Rotate(Vector3.forward * Time.deltaTime * 200f);
			yield return null;
		}
		transform.localPosition = posI;
	}
}
